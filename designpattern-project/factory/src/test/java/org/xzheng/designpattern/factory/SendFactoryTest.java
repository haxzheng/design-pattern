package org.xzheng.designpattern.factory;

import javax.annotation.Resource;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.xzheng.designpattern.factory.impl.MailSender;
import org.xzheng.designpattern.factory.impl.SmsSender;

@ContextConfiguration(locations = "classpath*:applicationContext.xml")
public class SendFactoryTest extends AbstractTestNGSpringContextTests {
	@Resource
	private AbstractFactoryApp abstractFactoryApp;

	@Test
	public void testSendFactory() {
		System.out.println("普通工厂");
		SendFactory factory = new SendFactory();
		ISender sender = factory.produce("sms");
		sender.send();
		Assert.assertEquals(SmsSender.class, sender.getClass());
	}

	@Test
	public void testMutilateMethodSendFactory() {
		System.out.println("多个工厂方法模式");
		MutilateMethodSendFactory factory = new MutilateMethodSendFactory();
		ISender sender = factory.produceMail();
		sender.send();
		Assert.assertEquals(MailSender.class, sender.getClass());
	}

	@Test
	public void testStaticMethodSendFactory() {
		System.out.println("静态工厂方法模式");
		ISender sender = StaticMethodSendFactory.produceMail();
		sender.send();
		Assert.assertEquals(MailSender.class, sender.getClass());
	}

	@Test
	public void testAbstractSendFactory() {
		System.out.println("抽象工厂模式");
		int count = abstractFactoryApp.sendmsg();
		Assert.assertEquals(count, 2);
	}

}
