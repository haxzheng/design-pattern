package org.xzheng.designpattern.factory;

import org.xzheng.designpattern.factory.impl.MailSender;
import org.xzheng.designpattern.factory.impl.SmsSender;

/**
 * 普通工厂模式
 * <p>
 * 是建立一个工厂类，对实现了同一接口的一些类进行实例的创建。
 * 
 * @author xzheng
 *
 */
public class SendFactory {
	public ISender produce(String name) {
		if ("mail".equals(name)) {
			return new MailSender();
		} else if ("sms".equals(name)) {
			return new SmsSender();
		} else {
			return null;
		}
	}
}
