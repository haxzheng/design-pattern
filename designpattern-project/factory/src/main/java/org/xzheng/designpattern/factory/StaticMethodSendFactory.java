package org.xzheng.designpattern.factory;

import org.xzheng.designpattern.factory.impl.MailSender;
import org.xzheng.designpattern.factory.impl.SmsSender;

/**
 * 静态工厂方法模式
 * <p>
 * 将MutilateMethodSendFactory多个工厂方法模式里的方法置为静态的，不需要创建实例，直接调用即可。
 * 
 * @author xzheng
 *
 */
public class StaticMethodSendFactory {
	public static ISender produceMail() {

		return new MailSender();
	}

	public static ISender produceSms() {
		return new SmsSender();
	}
}
