package org.xzheng.designpattern.factory;

import java.util.List;

public class AbstractFactoryApp {
	private List<IProvider> providers;

	public int sendmsg() {
		int count = 0;
		for (IProvider provider : providers) {
			provider.produce().send();
			count++;
		}
		return count;
	}

	public List<IProvider> getProviders() {
		return providers;
	}

	public void setProviders(List<IProvider> providers) {
		this.providers = providers;
	}
}
