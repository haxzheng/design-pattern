package org.xzheng.designpattern.factory;

import org.xzheng.designpattern.factory.impl.MailSender;

public class SendMailFactory implements IProvider {

	@Override
	public ISender produce() {
		return new MailSender();
	}
}
