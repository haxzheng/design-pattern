package org.xzheng.designpattern.factory;

import org.xzheng.designpattern.factory.impl.SmsSender;

public class SendSmsFactory implements IProvider {

	@Override
	public ISender produce() {
		return new SmsSender();
	}
}
