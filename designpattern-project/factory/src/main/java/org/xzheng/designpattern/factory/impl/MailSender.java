package org.xzheng.designpattern.factory.impl;

import org.xzheng.designpattern.factory.ISender;

public class MailSender implements ISender {
	@Override
	public void send() {
		System.out.println("This is MailSender!");
	}
}
