package org.xzheng.designpattern.factory;

public interface ISender {
	public void send();
}
