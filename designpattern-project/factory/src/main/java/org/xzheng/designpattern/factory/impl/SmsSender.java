package org.xzheng.designpattern.factory.impl;

import org.xzheng.designpattern.factory.ISender;

public class SmsSender implements ISender {

	@Override
	public void send() {
		System.out.println("This is SmsSender!");
	}
}
