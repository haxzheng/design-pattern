package org.xzheng.designpattern.singleton;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSerializable {
	@Test
	public void testSingletonWithoutReadResolve() {
		SynchronizedDoubleCheckSingleton instance = SynchronizedDoubleCheckSingleton.getInstance();
		String name = "org.xzheng.designpattern.singleton.SynchronizedDoubleCheckSingleton";
		try {
			FileOutputStream fo = new FileOutputStream(name);
			ObjectOutputStream os = new ObjectOutputStream(fo);
			os.writeObject(instance);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		SynchronizedDoubleCheckSingleton instance2 = null;
		try {
			ObjectInputStream oi = new ObjectInputStream(new FileInputStream(name));
			instance2 = (SynchronizedDoubleCheckSingleton) oi.readObject();
			oi.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(instance.hashCode(), instance2.hashCode());
	}

	@Test
	public void testSingletonWithReadResolve() {
		VolatileDoubleCheckSingleton instance = VolatileDoubleCheckSingleton.getInstance();
		String name = "org.xzheng.designpattern.singleton.VolatileDoubleCheckSingleton";
		try {
			FileOutputStream fo = new FileOutputStream(name);
			ObjectOutputStream os = new ObjectOutputStream(fo);
			os.writeObject(instance);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		VolatileDoubleCheckSingleton instance2 = null;
		try {
			ObjectInputStream oi = new ObjectInputStream(new FileInputStream(name));
			instance2 = (VolatileDoubleCheckSingleton) oi.readObject();
			oi.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(instance.hashCode(), instance2.hashCode());
	}
	
	@Test
	public void testEnumSingleton() {
		EnumSingleton instance = EnumSingleton.INSTANCE;
		String name = "org.xzheng.designpattern.singleton.EnumSingleton";
		try {
			FileOutputStream fo = new FileOutputStream(name);
			ObjectOutputStream os = new ObjectOutputStream(fo);
			os.writeObject(instance);
			os.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		EnumSingleton instance2 = null;
		try {
			ObjectInputStream oi = new ObjectInputStream(new FileInputStream(name));
			instance2 = (EnumSingleton) oi.readObject();
			oi.close();
		} catch (Exception e) {
			e.printStackTrace();
		}

		Assert.assertEquals(instance.hashCode(), instance2.hashCode());
	}
}
