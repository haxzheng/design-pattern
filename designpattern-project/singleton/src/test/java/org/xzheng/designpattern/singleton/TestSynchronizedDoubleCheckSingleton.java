package org.xzheng.designpattern.singleton;

import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import org.testng.Assert;
import org.testng.annotations.Test;

public class TestSynchronizedDoubleCheckSingleton {
	private static final int TESTCOUNT = 100;

	@Test
	public void f() {

		ExecutorService es = Executors.newFixedThreadPool(TESTCOUNT);
		final CountDownLatch cw = new CountDownLatch(TESTCOUNT);
		final Map<String, Integer> rlt = new ConcurrentHashMap<String, Integer>();
		for (int i = 0; i < TESTCOUNT; i++) {
			es.submit(new Callable<Object>() {
				@Override
				public Object call() throws Exception {
					Object ret = SynchronizedDoubleCheckSingleton.getInstance();
					rlt.put(ret.toString(), 1);
					cw.countDown();
					return ret;
				}
			});
		}

		try {
			cw.await();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertEquals(rlt.size(), 1);
		es.shutdown();

	}
}
