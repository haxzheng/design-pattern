package org.xzheng.designpattern.singleton;

/**
 * 单例模式
 * <p>
 * 懒汉模式，线程安全
 * <p>
 * 性能较差。当对象已经创建成功，之后的所有getInstance()方法是不需要进行同步的。
 * 
 * @author xzheng
 *
 */
public class LazyManThreadSafeSingleton {
	private static LazyManThreadSafeSingleton instance;

	private LazyManThreadSafeSingleton() {
	}

	public static synchronized LazyManThreadSafeSingleton getInstance() {
		if (instance == null) {
			instance = new LazyManThreadSafeSingleton();
		}
		return instance;
	}
}
