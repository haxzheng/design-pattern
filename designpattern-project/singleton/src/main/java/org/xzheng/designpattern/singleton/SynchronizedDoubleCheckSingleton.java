package org.xzheng.designpattern.singleton;

import java.io.Serializable;

/**
 * 单例模式
 * <p>
 * 双重校验锁
 * <p>
 * 内部静态方法syncInit()结束时会保证对象初始化完成，解决编译器优化问题.
 * @author xzheng
 *
 */
public class SynchronizedDoubleCheckSingleton implements Serializable {
	private static final long serialVersionUID = 1L;
	private static SynchronizedDoubleCheckSingleton instance;

	private SynchronizedDoubleCheckSingleton() {
	}

	private static synchronized void syncInit() {
		if (instance == null) {
			instance = new SynchronizedDoubleCheckSingleton();
		}
	}

	public static SynchronizedDoubleCheckSingleton getInstance() {
		if (instance == null) {
			syncInit();
		}
		return instance;
	}

	public Object readResolve() {
		return instance;
	}
}
