package org.xzheng.designpattern.singleton;

import java.io.Serializable;

/**
 * 单例模式
 * <p>
 * 双重校验锁
 * <p>
 * 编译器优化，可能导致问题：先给对象申请内存，再赋值给instance，但还未执行初始化操作，导致另外一个线程取到一个未初始化的对象。
 * 该问题在不同的编译器下表现可能会不一样。
 * 
 * @author xzheng
 *
 */
public class DoubleCheckSingleton implements Serializable {
	private static final long serialVersionUID = 1L;
	private static DoubleCheckSingleton instance;

	private DoubleCheckSingleton() {
	}

	public static DoubleCheckSingleton getInstance() {
		if (instance == null) {
			synchronized (DoubleCheckSingleton.class) {
				if (instance == null) {
					instance = new DoubleCheckSingleton();
				}
			}
		}
		return instance;
	}

	public Object readResolve() {
		return instance;
	}
}
