package org.xzheng.designpattern.singleton;

import java.io.Serializable;

/**
 * 单例模式
 * <p>
 * 枚举
 * <p>
 * 这种方式是Effective Java作者Josh Bloch
 * 提倡的方式，它不仅能避免多线程同步问题，而且还能防止反序列化重新创建新的对象。
 * JDK1.5以上版本支持
 * 
 * @author xzheng
 *
 */
public enum EnumSingleton implements Serializable{
	INSTANCE;
	
	private EnumSingleton(){
		System.out.println("init");
	}
	
	public void doBusiness() {
		System.out.println("working");
	}
	
	public static void main(String[] args) {
		EnumSingleton.INSTANCE.doBusiness();
	}
}
