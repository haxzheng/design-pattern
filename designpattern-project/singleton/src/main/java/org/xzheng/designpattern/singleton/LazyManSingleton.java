package org.xzheng.designpattern.singleton;

/**
 * 单例模式
 * <p>
 * 懒汉模式，线程不安全
 * 
 * @author xzheng
 *
 */
public class LazyManSingleton {
	private static LazyManSingleton instance = null;

	private LazyManSingleton() {
	}

	public static LazyManSingleton getInstance() {
		if (instance == null) {
			instance = new LazyManSingleton();
		}
		return instance;
	}
}
