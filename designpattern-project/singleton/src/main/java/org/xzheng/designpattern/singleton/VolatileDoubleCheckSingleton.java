package org.xzheng.designpattern.singleton;

import java.io.Serializable;

/**
 * 单例模式
 * <p>
 * 双重校验锁
 * <p>
 * volatile静态属性，防止编译器优化。仅支持JDK1.5之后版本。
 * @author xzheng
 *
 */
public class VolatileDoubleCheckSingleton implements Serializable {
	private static final long serialVersionUID = 1L;
	private static volatile VolatileDoubleCheckSingleton instance;

	private VolatileDoubleCheckSingleton() {
	}

	public static VolatileDoubleCheckSingleton getInstance() {
		if (instance == null) {
			synchronized (VolatileDoubleCheckSingleton.class) {
				if (instance == null) {
					instance = new VolatileDoubleCheckSingleton();
				}
			}
		}
		return instance;
	}
}
